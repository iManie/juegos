var idImg = "";
var imgAbierta = "";
var count = 0;
var pares =  0;
var tiempoId = 0;

function inst(){

    alert("Da click en los cuadrados y encuentra las imágenes iguales, presiona -Reiniciar- para empezar de nuevo.");
}


function contadorTiempo()
{
	var tiempo = $("#timer").html();
	tiempo++;
	$("#timer").html("" + tiempo);
	if (pares<10)
	{
		tiempoId = setTimeout('contadorTiempo()', 1000);
	}
}

function randomDeHasta(de, hasta){
    return Math.floor(Math.random() * (hasta - de + 1) + de);
}

function mezclar() {
    var imagenes = $("#imagenes").children();
    var imagen = $("#imagenes div:first-child");

    var array_img = new Array();

    for (i=0; i<imagenes.length; i++) {
        array_img[i] = $("#"+imagen.attr("id")+" img").attr("src");
        imagen = imagen.next();
    }

    var imagen = $("#imagenes div:first-child");

    for (z=0; z<imagenes.length; z++) {
        randIndex = randomDeHasta(0, array_img.length - 1);

       
        $("#"+imagen.attr("id")+" img").attr("src", array_img[randIndex]);
        array_img.splice(randIndex, 1);

        imagen = imagen.next();
    }
}

function reiniciarJuego() {
    mezclar();
    $("img").hide();
    $("img").removeClass("opacity");
    count = 0;
    $("#msg1").remove();
    $("#msg2").remove();
    $("#contador").html("" + count);
    idImg = "";
    imgAbierta = "";
    pares = 0;
    band = 0;
    $("#timer").html("0");
    clearTimeout(tiempoId);
    return false;
}

$(document).ready(function() {
    $("img").hide();
    $("#imagenes div").click(abrirImagen);

    mezclar();
   

    function abrirImagen() {
    	
    	

        id = $(this).attr("id");
		url = $("#"+id+" img").attr("src");
		
		
        

        if ($("#"+id+" img").is(":hidden")) {
            $("#imagenes div").unbind("click", abrirImagen);

            $("#"+id+" img").slideDown('fast');
            
            if (count == 0) {
			contadorTiempo();
		}

            if (imgAbierta == "") {
                idImg = id;
                imgAbierta = $("#"+id+" img").attr("src");
                setTimeout(function() {
                    $("#imagenes div").bind("click", abrirImagen)
                }, 300);
				
				pos = 1;
				val = 0;

            } else {
				pos = 2;
                actual = $("#"+id+" img").attr("src");
				val = 0;
                if (imgAbierta != actual) {
                    setTimeout(function() {
                        $("#"+id+" img").slideUp('fast');
                        $("#"+idImg+" img").slideUp('fast');
                        idImg = "";
                        imgAbierta = "";
                    }, 400);
                } else {
					val = 1;
                    $("#"+id+" img").addClass("opacity");
                    $("#"+idImg+" img").addClass("opacity");
                    pares++;
                    idImg = "";
                    imgAbierta = "";
                }
				
				

                setTimeout(function() {
                    $("#imagenes div").bind("click", abrirImagen)
                }, 400);
            }
            count++; 
			
			$.ajax({
             method: "POST",
  url: "ajax/memorama.php",
  data: { id: url, pos: pos, val: val }
})

            $("#contador").html("" + count);
            if (pares == 10) {
                msg = '<span id="msg1">Has finalizado el juego en </span>';
                $("#msgContador").prepend(msg);
                msg = '<span id="msg2">Has finalizado el juego en </span>';
                $("#msgTimer").prepend(msg);
            }
            
            
        } 
        
        

    }
    
}    );
 


